package com.zeitheron.hammermetals.init;

import com.zeitheron.hammercore.utils.recipes.helper.RecipeRegistry;
import com.zeitheron.hammercore.utils.recipes.helper.RegisterRecipes;
import com.zeitheron.hammermetals.InfoHM;
import com.zeitheron.hammermetals.api.HMLog;
import com.zeitheron.hammermetals.api.MetalData;
import com.zeitheron.hammermetals.api.MetalRegistry;
import com.zeitheron.hammermetals.cfg.ConfigsHM;
import com.zeitheron.hammermetals.recipes.RecipeMakeDust;
import com.zeitheron.hammermetals.recipes.RecipeMakePlate;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.ResourceLocation;

@RegisterRecipes(modid = InfoHM.MOD_ID)
public class RecipesHM extends RecipeRegistry
{
	@Override
	public void crafting()
	{
		if(!ConfigsHM.master_addRecipes)
			return;
		
		shaped(ItemsHM.HAMMER, " ii", " si", "s  ", 's', "stickIron", 'i', "ingotIron");
		recipe(new RecipeMakePlate());
		recipe(new RecipeMakeDust());
		
		MetalRegistry.metalSet().forEach(m ->
		{
			MetalData data = MetalRegistry.getMetal(m);
			if(data != null && data.enable.getAsBoolean())
			{
				HMLog.LOG.info("Processing recipes for metal type " + m + "...");
				data.addRecipes(this);
			} else
				HMLog.LOG.warn("Skipping addRecipe for metal type " + m + "!");
		});
	}
	
	@Override
	public void smelting()
	{
	}
	
	@Override
	public void shaped(Block out, Object... recipeComponents)
	{
		super.shaped(out, recipeComponents);
	}
	
	@Override
	public void shaped(Item out, Object... recipeComponents)
	{
		super.shaped(out, recipeComponents);
	}
	
	@Override
	public void shaped(ItemStack out, Object... recipeComponents)
	{
		super.shaped(out, recipeComponents);
	}
	
	@Override
	public void shapeless(Block out, Object... recipeComponents)
	{
		super.shapeless(out, recipeComponents);
	}
	
	@Override
	public void shapeless(Item out, Object... recipeComponents)
	{
		super.shapeless(out, recipeComponents);
	}
	
	@Override
	public void shapeless(ItemStack out, Object... recipeComponents)
	{
		super.shapeless(out, recipeComponents);
	}
	
	@Override
	protected String getMod()
	{
		return InfoHM.MOD_ID;
	}
	
	@Override
	protected void recipe(IRecipe recipe)
	{
		if(recipe.getRegistryName() == null)
			recipe = recipe.setRegistryName(new ResourceLocation("hammercore", "recipes." + getMod() + "." + getClass().getSimpleName() + "." + recipes.size()));
		recipes.add(recipe);
	}
}