package com.zeitheron.hammermetals.init;

import net.minecraft.block.Block;
import net.minecraft.block.material.Material;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;

public class BlocksHM
{
	public static final Block TEMPLATE_ORE = new Block(Material.ROCK)
	{
		{
			setUnlocalizedName("template_ore");
		}
		
		@Override
		public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
		{
		}
	};
}