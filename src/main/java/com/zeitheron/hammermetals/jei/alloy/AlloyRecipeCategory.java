package com.zeitheron.hammermetals.jei.alloy;

import com.zeitheron.hammercore.client.utils.UV;
import com.zeitheron.hammermetals.InfoHM;
import com.zeitheron.hammermetals.jei.JeiHM;

import mezz.jei.api.IJeiHelpers;
import mezz.jei.api.gui.IDrawable;
import mezz.jei.api.gui.IGuiItemStackGroup;
import mezz.jei.api.gui.IRecipeLayout;
import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeCategory;
import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class AlloyRecipeCategory implements IRecipeCategory<AlloyRecipeWrapper>
{
	final ResourceLocation jeirl = new ResourceLocation(InfoHM.MOD_ID, "textures/jei.png");
	final UV arrow = new UV(jeirl, 179, 18, 22, 15);
	final UV slot = new UV(jeirl, 179, 0, 18, 18);
	IDrawable bg, icon;
	
	public AlloyRecipeCategory(IJeiHelpers reg)
	{
		bg = reg.getGuiHelper().createDrawable(jeirl, 0, 0, 179, 22);
		try
		{
			icon = (IDrawable) Class.forName("mezz.jei.gui.elements.DrawableResource").getDeclaredConstructor(ResourceLocation.class, int.class, int.class, int.class, int.class, int.class, int.class, int.class, int.class, int.class, int.class).newInstance(new ResourceLocation("hammercore", "textures/items/manual.png"), 0, 0, 16, 16, 0, 0, 0, 0, 16, 16);
		} catch(Throwable err)
		{
			err.printStackTrace();
		}
	}
	
	@Override
	public String getUid()
	{
		return JeiHM.ALLOY_REGISTRY;
	}
	
	@Override
	public String getTitle()
	{
		return I18n.format("jei." + JeiHM.ALLOY_REGISTRY);
	}
	
	@Override
	public String getModName()
	{
		return InfoHM.MOD_NAME;
	}
	
	@Override
	public IDrawable getBackground()
	{
		return bg;
	}
	
	@Override
	public IDrawable getIcon()
	{
		return icon;
	}
	
	@Override
	public void drawExtras(Minecraft minecraft)
	{
		slot.render(157, 2);
	}
	
	@Override
	public void setRecipe(IRecipeLayout recipeLayout, AlloyRecipeWrapper recipeWrapper, IIngredients ingredients)
	{
		IGuiItemStackGroup items = recipeLayout.getItemStacks();
		
		items.init(0, false, 157, 2);
		items.set(0, recipeWrapper.recipe.getOutput());
		
		int i = 0;
		for(ItemStack item : recipeWrapper.recipe.getInputs())
		{
			items.init(1 + i, true, i * 18, 2);
			items.set(1 + i, item);
			++i;
		}
	}
}