package com.zeitheron.hammermetals.jei.alloy;

import java.util.ArrayList;
import java.util.List;

import mezz.jei.api.ingredients.IIngredients;
import mezz.jei.api.recipe.IRecipeWrapper;
import net.minecraft.client.Minecraft;
import net.minecraft.item.ItemStack;
import net.minecraft.util.text.TextFormatting;

public class AlloyRecipeWrapper implements IRecipeWrapper
{
	public final AlloyRecipe recipe;
	
	public AlloyRecipeWrapper(AlloyRecipe recipe)
	{
		this.recipe = recipe;
	}
	
	@Override
	public void getIngredients(IIngredients ingredients)
	{
		ingredients.setInputs(ItemStack.class, recipe.getInputs());
		ingredients.setOutput(ItemStack.class, recipe.getOutput());
	}
	
	final ThreadLocal<List<String>> TOOLTIP = ThreadLocal.withInitial(ArrayList::new);
	
	@Override
	public List<String> getTooltipStrings(int mouseX, int mouseY)
	{
		List<String> tooltip = TOOLTIP.get();
		tooltip.clear();
		
		if(!recipe.data.enable.getAsBoolean())
		{
			tooltip.add(TextFormatting.RED + "This recipe is disabled!");
		}
		
		return tooltip;
	}
	
	@Override
	public void drawInfo(Minecraft minecraft, int recipeWidth, int recipeHeight, int mouseX, int mouseY)
	{
		
	}
}