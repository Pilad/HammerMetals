package com.zeitheron.hammermetals.recipes;

import java.util.HashSet;
import java.util.Set;

import com.zeitheron.hammermetals.items.ItemHammer;

import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.util.NonNullList;
import net.minecraft.world.World;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistryEntry;

public class RecipeMakeDust extends IForgeRegistryEntry.Impl<IRecipe> implements IRecipe
{
	/**
	 * Add the material you want to exclude from hammer recipes. This applies
	 * directly, and can be changed at runtime to add plates that work under
	 * certain conditions.
	 */
	public static final Set<String> excludedMaterials = new HashSet<>();
	
	{
		setRegistryName("hammercore", "plate_dust");
	}
	
	@Override
	public boolean matches(InventoryCrafting inv, World worldIn)
	{
		boolean hammer = false;
		
		String selOd = null;
		int items = 0;
		
		for(int i = 0; i < inv.getSizeInventory(); ++i)
		{
			ItemStack stack = inv.getStackInSlot(i);
			if(!stack.isEmpty())
			{
				if(stack.getItem() instanceof ItemHammer)
				{
					if(!hammer)
						hammer = true;
					else
						return false;
				} else
				{
					boolean found = false;
					
					int[] ids = OreDictionary.getOreIDs(stack);
					for(int id : ids)
					{
						String od = OreDictionary.getOreName(id);
						if(od.startsWith("ingot"))
						{
							if(excludedMaterials.contains(od.substring(5).toLowerCase()))
								continue;
							if(OreDictionary.getOres("dust" + od.substring(5)).isEmpty())
								continue;
							
							if(selOd == null)
							{
								selOd = od;
								found = true;
								++items;
								break;
							} else if(selOd.equals(od))
							{
								found = true;
								++items;
								break;
							}
						}
					}
					
					if(!found)
						return false;
				}
			}
		}
		
		return hammer && items > 0 && items % 2 == 1;
	}
	
	@Override
	public ItemStack getCraftingResult(InventoryCrafting inv)
	{
		boolean hammer = false;
		
		String selOd = null;
		int items = 0;
		
		for(int i = 0; i < inv.getSizeInventory(); ++i)
		{
			ItemStack stack = inv.getStackInSlot(i);
			if(!stack.isEmpty())
			{
				if(stack.getItem() instanceof ItemHammer)
				{
					if(!hammer)
						hammer = true;
					else
						return ItemStack.EMPTY;
				} else
				{
					boolean found = false;
					
					int[] ids = OreDictionary.getOreIDs(stack);
					for(int id : ids)
					{
						String od = OreDictionary.getOreName(id);
						if(od.startsWith("ingot"))
						{
							if(excludedMaterials.contains(od.substring(5).toLowerCase()))
								continue;
							if(OreDictionary.getOres("dust" + od.substring(5)).isEmpty())
								continue;
							
							if(selOd == null)
							{
								selOd = od;
								found = true;
								++items;
								break;
							} else if(selOd.equals(od))
							{
								found = true;
								++items;
								break;
							}
						}
					}
					
					if(!found)
						return ItemStack.EMPTY;
				}
			}
		}
		
		if(hammer && items > 0 && items % 2 == 1)
		{
			NonNullList<ItemStack> stacks = OreDictionary.getOres("dust" + selOd.substring(5));
			if(!stacks.isEmpty())
			{
				ItemStack out = stacks.get(0).copy();
				out.setCount(items);
				return out;
			}
		}
		
		return ItemStack.EMPTY;
	}
	
	@Override
	public boolean canFit(int width, int height)
	{
		return width * height >= 3;
	}
	
	@Override
	public ItemStack getRecipeOutput()
	{
		return ItemStack.EMPTY;
	}
	
	@Override
	public boolean isHidden()
	{
		return true;
	}
}