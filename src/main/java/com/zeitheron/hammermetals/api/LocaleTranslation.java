package com.zeitheron.hammermetals.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.zeitheron.hammercore.lib.zlib.error.JSONException;
import com.zeitheron.hammercore.lib.zlib.json.JSONArray;
import com.zeitheron.hammercore.lib.zlib.json.JSONObject;
import com.zeitheron.hammercore.lib.zlib.json.serapi.IgnoreSerialization;
import com.zeitheron.hammercore.lib.zlib.json.serapi.Jsonable;
import com.zeitheron.hammercore.lib.zlib.json.serapi.SerializationContext;
import com.zeitheron.hammermetals.InfoHM;

import net.minecraft.client.resources.I18n;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class LocaleTranslation implements Jsonable
{
	@IgnoreSerialization
	public final Map<String, Map<String, String>> LOCALES = new HashMap<>();
	
	@Override
	public void deserializeJson(JSONObject json) throws JSONException
	{
		LOCALES.clear();
		LOCALES.putAll((Map) toMap(json));
	}
	
	@SideOnly(Side.CLIENT)
	public String getLocalization(String key)
	{
		Map<String, String> kv = LOCALES.get(key);
		if(kv == null)
			return key;
		return kv.getOrDefault(I18n.format("lang." + InfoHM.MOD_ID), kv.get("en"));
	}
	
	@Override
	public SerializationContext serializationContext()
	{
		SerializationContext ctx = new SerializationContext();
		for(String key : LOCALES.keySet())
		{
			JSONObject j = new JSONObject();
			Map<String, String> o = LOCALES.get(key);
			for(String k : o.keySet())
				try
				{
					j.putOpt(k, o.get(k));
				} catch(JSONException e)
				{
				}
			ctx.set(key, j);
		}
		return ctx;
	}
	
	public static List<Object> toList(JSONArray arr)
	{
		List<Object> list = new ArrayList<>();
		for(int i = 0; i < arr.length(); ++i)
		{
			Object o = arr.opt(i);
			if(o instanceof JSONArray)
				list.add(toList((JSONArray) o));
			else if(o instanceof JSONObject)
				list.add(toMap((JSONObject) o));
			else
				list.add(o);
		}
		return list;
	}
	
	public static Map<String, Object> toMap(JSONObject obj)
	{
		Map<String, Object> map = new HashMap<>();
		for(String key : obj.keySet())
		{
			Object o = obj.opt(key);
			if(o instanceof JSONObject)
				map.put(key, toMap((JSONObject) o));
			else if(o instanceof JSONArray)
				map.put(key, toList((JSONArray) o));
			else
				map.put(key, o);
		}
		return map;
	}
}