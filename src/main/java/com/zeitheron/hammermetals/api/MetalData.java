package com.zeitheron.hammermetals.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.function.BooleanSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;

import javax.annotation.Nullable;

import com.zeitheron.hammercore.internal.init.ItemsHC;
import com.zeitheron.hammercore.world.gen.WorldGenFeatureOre;
import com.zeitheron.hammercore.world.gen.WorldRetroGen;
import com.zeitheron.hammermetals.HammerMetals;
import com.zeitheron.hammermetals.InfoHM;
import com.zeitheron.hammermetals.api.parts.EnumBlockMetalPart;
import com.zeitheron.hammermetals.api.parts.EnumItemMetalPart;
import com.zeitheron.hammermetals.cfg.ConfigsHM;
import com.zeitheron.hammermetals.init.RecipesHM;
import com.zeitheron.hammermetals.items.ItemParametered;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.Item;
import net.minecraft.item.ItemBlock;
import net.minecraft.item.ItemStack;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.text.translation.I18n;
import net.minecraft.world.biome.Biome;
import net.minecraftforge.fml.common.registry.ForgeRegistries;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;

public class MetalData
{
	public final String id, idUpper, mod;
	
	public final MetalInfo info;
	
	public Map<EnumItemMetalPart, ItemParametered<Boolean>> itemParts = new HashMap<>();
	public Map<EnumBlockMetalPart, Block> blockParts = new HashMap<>();
	
	@Nullable
	public Block ore;
	
	@Nullable
	public WorldGenFeatureOre feature;
	
	public BooleanSupplier enable;
	
	public MetalData(MetalInfo info, Predicate<MetalInfo> enable)
	{
		this.enable = () -> enable.test(info);
		this.info = info;
		this.id = info.id;
		this.mod = info.mod;
		String upper = info.id;
		upper = Character.toUpperCase(upper.charAt(0)) + upper.substring(1);
		this.idUpper = upper;
		
		HMLog.LOG.info(" - " + upper + " >> " + info.serialize());
		
		for(EnumItemMetalPart part : EnumItemMetalPart.values())
			if(info.add(part))
				itemParts.put(part, registerItem(info.mod, info.id + "_" + part.sub, part.od + upper, part).setEnable(this.enable));
			
		for(EnumBlockMetalPart part : info.blockParts)
			if(part.notOre())
			{
				Block b = registerBlock(info.mod, info.id + "_" + part.sub, part.od + upper, part);
				b.setHardness(4F);
				b.setHarvestLevel("pickaxe", 2);
				blockParts.put(part, b);
			}
		
		if(info.add(EnumBlockMetalPart.ORE))
		{
			ore = registerBlock(info.mod, info.id + "_ore", "ore" + upper, EnumBlockMetalPart.ORE);
			ore.setHardness(2F);
			ore.setHarvestLevel("pickaxe", 1);
			
			blockParts.put(EnumBlockMetalPart.ORE, ore);
			
			if(ConfigsHM.master_addOreGen && this.enable.getAsBoolean())
			{
				WorldGenFeatureOre f = feature = new WorldGenFeatureOre(ore.getDefaultState());
				f.maxCusterSize = info.maxVeinSize;
				f.maxClusters = info.maxInChunk;
				f.minY = info.minY;
				f.maxY = info.maxY;
				
				WorldGenInfo wgi = info.gen;
				
				if(wgi != null)
				{
					HMLog.LOG.info(" - Compiling worldgen...");
					
					GenSource src = wgi.gen_src;
					if(src != null)
					{
						IBlockState s = src.asState();
						if(s != null)
							f.sourceState = s;
						HMLog.LOG.info("   - Generates in IBlockState: " + s);
					}
					
					if(wgi.biomeWhitelist)
					{
						IForgeRegistry<Biome> bioreg = GameRegistry.findRegistry(Biome.class);
						feature.enableBiomeWhitelist = true;
						List<String> bios = wgi.biomes;
						if(bios != null && bios.size() > 0)
							for(int i = 0; i < bios.size(); ++i)
							{
								String bio = bios.get(i);
								if(!bio.isEmpty())
									feature.biomes.add(bioreg.getValue(new ResourceLocation(bio)));
							}
						HMLog.LOG.info("   - Generates in Biomes: " + bios);
					}
					
					if(wgi.dimensionWhitelist)
					{
						feature.enableDimensionWhitelist = true;
						List<Integer> ds = wgi.dimensions;
						if(ds != null && ds.size() > 0)
							for(int i = 0; i < ds.size(); ++i)
								feature.dimensionWhitelist.add(ds.get(i));
						HMLog.LOG.info("   - Generates in Dimensions: " + ds);
					}
				}
				
				WorldRetroGen.addWorldFeature(f.setRegistryName(InfoHM.MOD_ID, "ore_" + id));
				HMLog.LOG.info(" - World Gen Registered!");
			} else if(info.gen != null)
				HMLog.LOG.info(" - World Gen Disabled!");
		}
		
		if(ConfigsHM.master_addRecipes && this.enable.getAsBoolean())
		{
			if(has(EnumItemMetalPart.INGOT) && has(EnumItemMetalPart.DUST))
				GameRegistry.addSmelting(get(EnumItemMetalPart.DUST), new ItemStack(get(EnumItemMetalPart.INGOT)), .1F);
			
			if(has(EnumItemMetalPart.INGOT) && has(EnumItemMetalPart.PLATE))
				GameRegistry.addSmelting(get(EnumItemMetalPart.PLATE), new ItemStack(get(EnumItemMetalPart.INGOT)), 0F);
			
			if(has(EnumItemMetalPart.INGOT) && has(EnumItemMetalPart.GEAR))
				GameRegistry.addSmelting(get(EnumItemMetalPart.GEAR), new ItemStack(get(EnumItemMetalPart.INGOT), 4), 1F);
			
			if(has(EnumItemMetalPart.INGOT) && ore != null)
				GameRegistry.addSmelting(ore, new ItemStack(get(EnumItemMetalPart.INGOT)), .1F);
			
			if(has(EnumItemMetalPart.INGOT) && has(EnumItemMetalPart.ROD))
				GameRegistry.addSmelting(get(EnumItemMetalPart.ROD), new ItemStack(get(EnumItemMetalPart.INGOT)), .1F);
		}
		
		if(info.onFinish != null)
			info.onFinish.accept(this);
	}
	
	@SideOnly(Side.CLIENT)
	public void registerModels()
	{
		if(info.useTextureTemplate)
		{
			String pref = "hammermetals:";
			
			getItems().stream().filter(item -> HammerMetals.isFromHM(item) && !(item instanceof ItemBlock)).forEach(item -> Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(item, 0, new ModelResourceLocation(pref + "tin_" + item.getRegistryName().getResourcePath().toLowerCase().replaceAll(id + "_", ""), "inventory")));
			
			for(EnumBlockMetalPart part : EnumBlockMetalPart.values())
				if(part.notOre() && has(part) && HammerMetals.isFromHM(get(part)))
					Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(get(part)), 0, new ModelResourceLocation(pref + "tin_" + part.sub, "inventory"));
			
			if(has(EnumBlockMetalPart.ORE) && HammerMetals.isFromHM(get(EnumBlockMetalPart.ORE)) && info.useOreTemplate)
				Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(get(EnumBlockMetalPart.ORE)), 0, new ModelResourceLocation(pref + "template_ore", "inventory"));
			if(has(EnumBlockMetalPart.ORE) && HammerMetals.isFromHM(get(EnumBlockMetalPart.ORE)) && !info.useOreTemplate)
				Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Item.getItemFromBlock(get(EnumBlockMetalPart.ORE)), 0, new ModelResourceLocation(get(EnumBlockMetalPart.ORE).getRegistryName(), "inventory"));
		}
	}
	
	public Item get(EnumItemMetalPart part)
	{
		if(!itemParts.containsKey(part))
		{
			Optional<Item> opt = OreDictionary.getOres(part.od + idUpper).stream().filter(stack -> stack.getItemDamage() == 0).map(s -> s.getItem()).findFirst();
			if(opt.isPresent())
				return opt.get();
		}
		return itemParts.get(part);
	}
	
	public Block get(EnumBlockMetalPart part)
	{
		if(!blockParts.containsKey(part))
		{
			Optional<Block> opt = OreDictionary.getOres(part.od + idUpper).stream().filter(stack -> stack.getItem() instanceof ItemBlock && stack.getItemDamage() == 0).map(s -> ((ItemBlock) s.getItem()).getBlock()).findFirst();
			if(opt.isPresent())
				return opt.get();
		}
		return blockParts.get(part);
	}
	
	public boolean hasAlloy()
	{
		return info.alloy != null;
	}
	
	public AlloyInfo getAlloy()
	{
		return info.alloy;
	}
	
	public boolean has(EnumItemMetalPart part)
	{
		return get(part) != null;
	}
	
	public boolean has(EnumBlockMetalPart part)
	{
		return get(part) != null;
	}
	
	@Deprecated
	public Item getIngot()
	{
		return get(EnumItemMetalPart.INGOT);
	}
	
	@Deprecated
	public boolean hasIngot()
	{
		return getIngot() != null;
	}
	
	@Deprecated
	public Item getDust()
	{
		return get(EnumItemMetalPart.DUST);
	}
	
	@Deprecated
	public boolean hasDust()
	{
		return getDust() != null;
	}
	
	@Deprecated
	public Item getGear()
	{
		return get(EnumItemMetalPart.GEAR);
	}
	
	@Deprecated
	public boolean hasGear()
	{
		return getGear() != null;
	}
	
	@Deprecated
	public Item getPlate()
	{
		return get(EnumItemMetalPart.PLATE);
	}
	
	@Deprecated
	public boolean hasPlate()
	{
		return getPlate() != null;
	}
	
	@Deprecated
	public Item getNugget()
	{
		return get(EnumItemMetalPart.NUGGET);
	}
	
	@Deprecated
	public boolean hasNugget()
	{
		return getNugget() != null;
	}
	
	@Deprecated
	public Block getBlock()
	{
		return get(EnumBlockMetalPart.BLOCK);
	}
	
	@Deprecated
	public boolean hasBlock()
	{
		return getBlock() != null;
	}
	
	@Deprecated
	public Block getOre()
	{
		return get(EnumBlockMetalPart.ORE);
	}
	
	@Deprecated
	public boolean hasOre()
	{
		return getOre() != null;
	}
	
	public void addItems(List<Item> items)
	{
		for(EnumItemMetalPart part : EnumItemMetalPart.values())
			if(has(part))
				items.add(get(part));
			
		for(EnumBlockMetalPart part : EnumBlockMetalPart.values())
			if(has(part))
				items.add(Item.getItemFromBlock(get(part)));
	}
	
	public void addBlocks(List<Block> blocks)
	{
		for(EnumBlockMetalPart part : EnumBlockMetalPart.values())
			if(has(part))
				blocks.add(get(part));
	}
	
	public List<Item> getItems()
	{
		List<Item> it = new ArrayList<>();
		addItems(it);
		return it;
	}
	
	public List<Block> getBlocks()
	{
		List<Block> it = new ArrayList<>();
		addBlocks(it);
		return it;
	}
	
	private ItemParametered<Boolean> registerItem(String modelMod, String unlocName, @Nullable String od, EnumItemMetalPart part)
	{
		ItemParametered<Boolean> i = new ItemParametered<>();
		
		if(info.useGeneratedLang)
		{
			i.sub = "lang." + InfoHM.MOD_ID + ":" + part.sub;
			i.material = () -> HammerMetals.proxy.getMaterial(info);
		}
		
		i.setUnlocalizedName(modelMod + ":" + unlocName);
		i.setRegistryName(InfoHM.MOD_ID, unlocName);
		i.setCreativeTab(HammerMetals.TAB);
		i.registerModel = !info.useTextureTemplate;
		ForgeRegistries.ITEMS.register(i);
		if(!info.useTextureTemplate)
			ItemsHC.items.add(i);
		if(od != null)
			OreDictionary.registerOre(od, i);
		return i;
	}
	
	private Block registerBlock(String modelMod, String unlocName, @Nullable String od, EnumBlockMetalPart part)
	{
		boolean ore = od.contains("ore");
		Block i = new Block(ore ? Material.ROCK : Material.IRON)
		{
			{
				setSoundType(ore ? SoundType.STONE : SoundType.METAL);
			}
			
			@Override
			public void getSubBlocks(CreativeTabs itemIn, NonNullList<ItemStack> items)
			{
				if(enable.getAsBoolean())
					super.getSubBlocks(itemIn, items);
			}
			
			@Override
			public BlockRenderLayer getBlockLayer()
			{
				return ore ? BlockRenderLayer.CUTOUT : BlockRenderLayer.SOLID;
			}
		};
		i.setUnlocalizedName(modelMod + ":" + unlocName);
		i.setRegistryName(InfoHM.MOD_ID, unlocName);
		i.setCreativeTab(HammerMetals.TAB);
		ForgeRegistries.BLOCKS.register(i);
		ForgeRegistries.ITEMS.register(new ItemBlock(i)
		{
			public String sub = "lang." + InfoHM.MOD_ID + ":" + part.sub;
			public Supplier<String> material = () -> HammerMetals.proxy.getMaterial(info);
			
			@Override
			public String getItemStackDisplayName(ItemStack stack)
			{
				if(info.useGeneratedLang && sub != null)
					return I18n.translateToLocalFormatted(sub, material.get());
				else
					return super.getItemStackDisplayName(stack);
			}
		}.setRegistryName(i.getRegistryName()).setUnlocalizedName(modelMod + ":" + unlocName));
		if(!info.useTextureTemplate)
			ItemsHC.items.add(Item.getItemFromBlock(i));
		if(od != null)
			OreDictionary.registerOre(od, i);
		return i;
	}
	
	public void addRecipes(RecipesHM reg)
	{
		blend: if(has(EnumItemMetalPart.DUST) && info.alloy != null)
		{
			for(String sub : info.alloy.metals)
				if(!OreDictionary.doesOreNameExist("dust" + Character.toUpperCase(sub.charAt(0)) + sub.substring(1)))
					break blend;
			List<Object> ingredients = new ArrayList<>();
			for(String sub : info.alloy.metals)
				ingredients.add("dust" + Character.toUpperCase(sub.charAt(0)) + sub.substring(1));
			Object[] in = ingredients.toArray(new Object[ingredients.size()]);
			reg.shapeless(new ItemStack(get(EnumItemMetalPart.DUST), info.alloy.output), in);
		}
		
		if(has(EnumItemMetalPart.ROD) && OreDictionary.doesOreNameExist("ingot" + idUpper))
			reg.shaped(new ItemStack(get(EnumItemMetalPart.ROD), 2), "i", "i", 'i', "ingot" + idUpper);
		
		if(has(EnumItemMetalPart.COIL) && OreDictionary.doesOreNameExist("stick" + idUpper))
			reg.shaped(get(EnumItemMetalPart.COIL), "rwr", "iii", "rwr", 'r', "dustRedstone", 'w', "slabWood", 'i', "stick" + idUpper);
		
		if(has(EnumBlockMetalPart.CASING) && OreDictionary.doesOreNameExist("ingot" + idUpper) && OreDictionary.doesOreNameExist("gear" + idUpper))
			reg.shaped(new ItemStack(get(EnumBlockMetalPart.CASING)), "igi", "grg", "igi", 'i', "ingot" + idUpper, 'g', "gear" + idUpper, 'r', "dustRedstone");
		
		if(has(EnumItemMetalPart.INGOT))
		{
			if(!OreDictionary.getOres("block" + idUpper).isEmpty())
				reg.shapeless(new ItemStack(get(EnumItemMetalPart.INGOT), 9), "block" + idUpper);
			if(!OreDictionary.getOres("nugget" + idUpper).isEmpty())
				reg.shapeless(get(EnumItemMetalPart.INGOT), "nugget" + idUpper, "nugget" + idUpper, "nugget" + idUpper, "nugget" + idUpper, "nugget" + idUpper, "nugget" + idUpper, "nugget" + idUpper, "nugget" + idUpper, "nugget" + idUpper);
		}
		
		if(has(EnumBlockMetalPart.BLOCK) && !OreDictionary.getOres("ingot" + idUpper).isEmpty())
			reg.shapeless(get(EnumBlockMetalPart.BLOCK), "ingot" + idUpper, "ingot" + idUpper, "ingot" + idUpper, "ingot" + idUpper, "ingot" + idUpper, "ingot" + idUpper, "ingot" + idUpper, "ingot" + idUpper, "ingot" + idUpper);
		
		if(has(EnumItemMetalPart.NUGGET) && !OreDictionary.getOres("ingot" + idUpper).isEmpty())
			reg.shapeless(new ItemStack(get(EnumItemMetalPart.NUGGET), 9), "ingot" + idUpper);
		
		if(has(EnumItemMetalPart.GEAR) && !OreDictionary.getOres("ingot" + idUpper).isEmpty())
		{
			reg.shaped(get(EnumItemMetalPart.GEAR), " m ", "mcm", " m ", 'm', "ingot" + idUpper, 'c', "ingotCopper");
			reg.shaped(get(EnumItemMetalPart.GEAR), " m ", "mcm", " m ", 'm', "ingot" + idUpper, 'c', "ingotIron");
		}
	}
}