package com.zeitheron.hammermetals.api;

import java.util.List;

import com.zeitheron.hammercore.lib.zlib.json.serapi.Jsonable;

public class AlloyInfo implements Jsonable
{
	public List<String> metals;
	public int output;
	
	@Override
	public String serialize()
	{
		String it = "[";
		
		for(String m : metals)
			it += "\"" + Jsonable.formatInsideString(m) + "\",";
		
		if(it.endsWith(","))
			it = it.substring(0, it.length() - 1);
		
		return "{\"output\":" + output + ",\"inputs\":" + it + "}";
	}
}