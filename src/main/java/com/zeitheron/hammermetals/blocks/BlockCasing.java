package com.zeitheron.hammermetals.blocks;

import net.minecraft.block.Block;
import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockCasing extends Block
{
	public BlockCasing(String type)
	{
		super(Material.IRON);
		setUnlocalizedName("casing_" + type);
		setHardness(2F);
		setSoundType(SoundType.METAL);
	}
}