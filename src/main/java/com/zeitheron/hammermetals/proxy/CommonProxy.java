package com.zeitheron.hammermetals.proxy;

import com.zeitheron.hammermetals.api.MetalInfo;

public class CommonProxy
{
	public void init()
	{
	}
	
	public String getMaterial(MetalInfo info)
	{
		return info.idUpper();
	}
}